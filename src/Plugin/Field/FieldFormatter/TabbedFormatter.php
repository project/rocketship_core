<?php

namespace Drupal\rocketship_core\Plugin\Field\FieldFormatter;

use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Field\FormatterBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Plugin implementation of the 'tabbed_formatter' formatter.
 *
 * @FieldFormatter(
 *   id = "tabbed_formatter",
 *   label = @Translation("Tabbed field formatter"),
 *   field_types = {
 *     "tabbed_item"
 *   }
 * )
 */
class TabbedFormatter extends FormatterBase {

  /**
   * {@inheritdoc}
   */
  public static function defaultSettings() {
    return [
      'header' => 'h2',
    ] + parent::defaultSettings();
  }

  /**
   * {@inheritdoc}
   */
  public function settingsForm(array $form, FormStateInterface $form_state) {
    $form = parent::settingsForm($form, $form_state);

    $form['header'] = [
      '#type' => 'select',
      '#title' => $this->t('Header tag'),
      '#options' => $this->getOptions(),
      '#default_value' => $this->getSetting('header'),
      '#description' => $this->t('Define the header tag element that will be wrapped around this field.'),
    ];
    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function settingsSummary() {
    $summary = parent::settingsSummary();
    $summary[] = $this->t('Header element: @header', [
      '@header' => $this->getSetting('header'),
    ]);
    return $summary;
  }

  /**
   * Return the options for the select element.
   *
   * @return array
   *   The options array.
   */
  protected function getOptions() {
    return [
      'h1' => 'h1',
      'h2' => 'h2',
      'h3' => 'h3',
      'h4' => 'h4',
      'h5' => 'h5',
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function viewElements(FieldItemListInterface $items, $langcode) {
    $elements = [];
    $tag = $this->getSetting('header');

    foreach ($items as $delta => $item) {
      $id = implode('-', [
        $this->fieldDefinition->getName(),
        $items->getEntity()->id(),
        $delta,
      ]);

      $elements[$delta] = [
        '#type' => 'container',
        '#attributes' => [
          'class' => ['tab-item'],
        ],
        'title' => [
          '#type' => 'html_tag',
          '#tag' => $tag,
          '#attributes' => [
            'class' => ['tab-item__title'],
          ],
          'title_button' => [
            '#type' => 'html_tag',
            '#tag' => 'button',
            '#attributes' => [
              'aria-expanded' => 'false',
              'aria-controls' => 'faq-content-' . $id,
              'class' => [
                'tab-item__button',
              ],
              'id' => 'faq-' . $id,
            ],
            '#value' => $item->title,
          ],
        ],
        'value' => [
          '#type' => 'html_tag',
          '#tag' => 'div',
          '#attributes' => [
            'id' => 'faq-content-' . $id,
            'role' => 'region',
            'labelledby' => 'faq-' . $id,
            'class' => [
              'tab-item__content',
              'text-long',
            ],
          ],
          [
            '#type' => 'processed_text',
            '#text' => $item->value,
            '#format' => $item->format,
            '#langcode' => $item->getLangcode(),
          ],
        ],
      ];
    }

    return $elements;
  }

}
